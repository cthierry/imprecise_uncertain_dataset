The data made available on this git are free of rights.
However, we ask that the BELIEF 2022 conference paper be cited if the use of the data leads to a scientific communication.

C. Thierry, A. Hoarau, A.  Martin, JC. Dubois, Y. Le Gall. Real Bird Dataset with Imprecise and Uncertain Values. In: Le Hégarat-Mascle, S., Bloch, I., Aldea, E. (eds) Belief Functions: Theory and Applications. BELIEF 2022. Lecture Notes in Computer Science, Springer, Cham, vol 13506, pp 275–285, 2022. 
https://doi.org/10.1007/978-3-031-17801-6_26

The 10_birds_x folders contain responses from crowdsourcing campaigns in which only 10 bird species are presented to respondents. Multi_birds_x folders contain responses from crowdsourcing campaigns in which a larger number of bird species are presented to respondents. The birds_img folder includes all the bird photos used for the crowdsourcing campaigns (note that some photos are common to the campaigns, others are not).
The photos collected for the campaigns come from our contacts and Pixabay collaborators. We would like to thank the photographers who have allowed us to use their images.